package com.ff.multiple_connection.controller;

import com.ff.multiple_connection.models.DatabaseDetails;
import com.ff.multiple_connection.models.dto.CompareRequestDto;
import com.ff.multiple_connection.service.DynamicDataSourceService;
import com.ff.multiple_connection.service.SchemaComparator;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/schema")
@RequiredArgsConstructor
public class SchemaController {
    private final DynamicDataSourceService dynamicDataSourceService;
    private final SchemaComparator schemaComparator;

    @PostMapping("/compare")
    public String compareSchemas(@RequestBody CompareRequestDto compareRequestDto) {
        JdbcTemplate sourceJdbcTemplate = new JdbcTemplate(dynamicDataSourceService.createDataSource(compareRequestDto.getSourceDetails()));
        JdbcTemplate targetJdbcTemplate = new JdbcTemplate(dynamicDataSourceService.createDataSource(compareRequestDto.getTargetDetails()));

        schemaComparator.compareSchemas(sourceJdbcTemplate, targetJdbcTemplate, compareRequestDto.getSourceSchema(), compareRequestDto.getTargetSchema());
        return "Schema comparison complete.";
    }
}

