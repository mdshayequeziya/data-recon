package com.ff.multiple_connection.controller;

import com.ff.multiple_connection.models.DatabaseDetails;
import com.ff.multiple_connection.service.DataReconciliation;
import com.ff.multiple_connection.service.DynamicDataSourceService;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/data")
@RequiredArgsConstructor
public class DataController {


    private final DynamicDataSourceService dynamicDataSourceService;
    private final DataReconciliation dataReconciliation;

    @PostMapping("/reconcile")
    public String reconcileData(@RequestBody DatabaseDetails sourceDetails, @RequestBody DatabaseDetails targetDetails, @RequestParam String sourceTable, @RequestParam String targetTable) {
        JdbcTemplate sourceJdbcTemplate = new JdbcTemplate(dynamicDataSourceService.createDataSource(sourceDetails));
        JdbcTemplate targetJdbcTemplate = new JdbcTemplate(dynamicDataSourceService.createDataSource(targetDetails));

        dataReconciliation.reconcileData(sourceJdbcTemplate, targetJdbcTemplate, sourceTable, targetTable);
        return "Data reconciliation complete.";
    }
}

