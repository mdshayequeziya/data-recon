package com.ff.multiple_connection.models.dto;

import com.ff.multiple_connection.models.DatabaseDetails;
import lombok.Data;

@Data
public class CompareRequestDto {

    private DatabaseDetails sourceDetails;
    private DatabaseDetails targetDetails;
    private String sourceSchema;
    private String targetSchema;
}
