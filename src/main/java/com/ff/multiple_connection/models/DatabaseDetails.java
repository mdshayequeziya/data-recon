package com.ff.multiple_connection.models;

import lombok.Data;

@Data
public class DatabaseDetails {

    private String url;
    private String username;
    private String password;
    private String driverClassName;
}
