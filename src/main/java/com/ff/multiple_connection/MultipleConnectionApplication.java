package com.ff.multiple_connection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class MultipleConnectionApplication {

	public static void main(String[] args) {
		SpringApplication.run(MultipleConnectionApplication.class, args);
	}

}
