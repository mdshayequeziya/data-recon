package com.ff.multiple_connection.service;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class DataReconciliation {

    public List<Map<String, Object>> getData(JdbcTemplate jdbcTemplate, String tableName) {
        return jdbcTemplate.queryForList("SELECT * FROM " + tableName);
    }

    public void reconcileData(JdbcTemplate sourceJdbcTemplate, JdbcTemplate targetJdbcTemplate, String sourceTable, String targetTable) {
        List<Map<String, Object>> sourceData = getData(sourceJdbcTemplate, sourceTable);
        List<Map<String, Object>> targetData = getData(targetJdbcTemplate, targetTable);

        // Reconciliation logic here
    }
}
