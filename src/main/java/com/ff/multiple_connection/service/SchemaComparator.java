package com.ff.multiple_connection.service;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SchemaComparator {

    public List<Map<String, Object>> getTables(JdbcTemplate jdbcTemplate, String schemaName) {
        return jdbcTemplate.queryForList(
                "SELECT table_name FROM information_schema.tables WHERE table_schema = ? AND table_type = 'BASE TABLE'",
                schemaName
        );
    }

    public List<Map<String, Object>> getColumns(JdbcTemplate jdbcTemplate, String schemaName, String tableName) {
        return jdbcTemplate.queryForList(
                "SELECT column_name, data_type, is_nullable, column_default FROM information_schema.columns WHERE table_schema = ? AND table_name = ?",
                schemaName, tableName
        );
    }

    public void compareSchemas(JdbcTemplate sourceJdbcTemplate, JdbcTemplate targetJdbcTemplate, String sourceSchema, String targetSchema) {
        List<Map<String, Object>> sourceTables = getTables(sourceJdbcTemplate, sourceSchema);
        List<Map<String, Object>> targetTables = getTables(targetJdbcTemplate, targetSchema);

        // Convert lists to maps for easier comparison
        Map<String, Map<String, Object>> sourceTableMap = sourceTables.stream().collect(Collectors.toMap(
                table -> table.get("table_name").toString(),
                table -> table
        ));
        Map<String, Map<String, Object>> targetTableMap = targetTables.stream().collect(Collectors.toMap(
                table -> table.get("table_name").toString(),
                table -> table
        ));

        // Compare tables
        compareTableNames(sourceTableMap, targetTableMap);

        // Compare columns for each table
        for (String tableName : sourceTableMap.keySet()) {
            if (targetTableMap.containsKey(tableName)) {
                compareTableColumns(sourceJdbcTemplate, targetJdbcTemplate, sourceSchema, targetSchema, tableName);
            }
        }
    }

    private void compareTableNames(Map<String, Map<String, Object>> sourceTableMap, Map<String, Map<String, Object>> targetTableMap) {
        // Check for tables missing in the target schema
        for (String tableName : sourceTableMap.keySet()) {
            if (!targetTableMap.containsKey(tableName)) {
                System.out.println("Table " + tableName + " is missing in target schema.");
            }
        }

        // Check for extra tables in the target schema
        for (String tableName : targetTableMap.keySet()) {
            if (!sourceTableMap.containsKey(tableName)) {
                System.out.println("Extra table " + tableName + " found in target schema.");
            }
        }
    }

    private void compareTableColumns(JdbcTemplate sourceJdbcTemplate, JdbcTemplate targetJdbcTemplate, String sourceSchema, String targetSchema, String tableName) {
        List<Map<String, Object>> sourceColumns = getColumns(sourceJdbcTemplate, sourceSchema, tableName);
        List<Map<String, Object>> targetColumns = getColumns(targetJdbcTemplate, targetSchema, tableName);

        // Convert lists to maps for easier comparison
        Map<String, Map<String, Object>> sourceColumnMap = sourceColumns.stream().collect(Collectors.toMap(
                column -> column.get("column_name").toString(),
                column -> column
        ));
        Map<String, Map<String, Object>> targetColumnMap = targetColumns.stream().collect(Collectors.toMap(
                column -> column.get("column_name").toString(),
                column -> column
        ));

        // Check for columns missing in the target schema
        for (String columnName : sourceColumnMap.keySet()) {
            if (!targetColumnMap.containsKey(columnName)) {
                System.out.println("Column " + columnName + " in table " + tableName + " is missing in target schema.");
            } else {
                // Compare column details
                compareColumnDetails(sourceColumnMap.get(columnName), targetColumnMap.get(columnName), tableName);
            }
        }

        // Check for extra columns in the target schema
        for (String columnName : targetColumnMap.keySet()) {
            if (!sourceColumnMap.containsKey(columnName)) {
                System.out.println("Extra column " + columnName + " found in table " + tableName + " in target schema.");
            }
        }
    }

    private void compareColumnDetails(Map<String, Object> sourceColumn, Map<String, Object> targetColumn, String tableName) {
        String columnName = sourceColumn.get("column_name").toString();

        // Compare data types
        if (!sourceColumn.get("data_type").equals(targetColumn.get("data_type"))) {
            System.out.println("Data type mismatch for column " + columnName + " in table " + tableName + ". Source: " + sourceColumn.get("data_type") + ", Target: " + targetColumn.get("data_type"));
        }

        // Compare nullability
        if (!sourceColumn.get("is_nullable").equals(targetColumn.get("is_nullable"))) {
            System.out.println("Nullability mismatch for column " + columnName + " in table " + tableName + ". Source: " + sourceColumn.get("is_nullable") + ", Target: " + targetColumn.get("is_nullable"));
        }

        // Compare default values
        if (sourceColumn.get("column_default") != null ? !sourceColumn.get("column_default").equals(targetColumn.get("column_default")) : targetColumn.get("column_default") != null) {
            System.out.println("Default value mismatch for column " + columnName + " in table " + tableName + ". Source: " + sourceColumn.get("column_default") + ", Target: " + targetColumn.get("column_default"));
        }
    }
}
