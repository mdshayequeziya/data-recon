package com.ff.multiple_connection.service;


import com.ff.multiple_connection.models.DatabaseDetails;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Service
public class DynamicDataSourceService {

    private final Map<String, DataSource> dataSourceMap = new HashMap<>();

    public DataSource createDataSource(DatabaseDetails details) {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(details.getUrl());
        config.setUsername(details.getUsername());
        config.setPassword(details.getPassword());
        config.setDriverClassName(details.getDriverClassName());

        HikariDataSource dataSource = new HikariDataSource(config);
        dataSourceMap.put(details.getUrl(), dataSource);
        return dataSource;
    }

    public JdbcTemplate getJdbcTemplate(String url) {
        DataSource dataSource = dataSourceMap.get(url);
        if (dataSource == null) {
            throw new RuntimeException("Data source not found for URL: " + url);
        }
        return new JdbcTemplate(dataSource);
    }
}
